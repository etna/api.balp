package hr.etna.api.adapter;

import hr.etna.api.adapter.entities.BalpMessage;
import hr.etna.api.adapter.service.BalpService;
import hr.etna.api.adapter.service.BalpServiceImpl;
import hr.etna.api.adapter.util.BalpAdapterException;
import hr.etna.api.adapter.util.BalpHelper;

import java.util.Map;


public class Balp {

	private static BalpService service = null;

	private Balp() {
        // private constructor
    }

    public static BalpService service() throws BalpAdapterException {
        if (service == null) {
            service = new BalpServiceImpl();
		}
		return service;
	}

    public static void fire(BalpMessage msg) {
        try {
            service();
            System.out.println("CORE AIDIDID : " + msg.getCorrelationId());
            if (msg.getCorrelationId() == null || msg.getCorrelationId().length() == 0) {
                String coId = BalpHelper.createMessageId();
                msg.setCorrelationId(coId);
                System.out.println("[BALP] [Warning] BalpMessage 'correlationId' is empty. Adapter created id automaticly => correlationId: " + msg.getCorrelationId());
            }
            service.fire(msg);
        } catch (BalpAdapterException e) {
            System.err.println("[BALP ADAPTER] Error: " + e.getMessage());
        }
    }

    public static Map<String, BalpMessage> getFailedUnsentBalpMessages() {
        if (service != null)
            return service.getFailedUnsentBalpMessages();
        else
            return null;
    }

    public static boolean resetFailedMessages() {
        if (service != null) {
            service.resetFailedUnsentBalpMessages();
            return true;
        } else {
            return false;
        }
    }
}
