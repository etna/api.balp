package hr.etna.api.adapter.service;

import hr.etna.api.adapter.Balp;
import hr.etna.api.adapter.entities.BalpMessage;
import hr.etna.api.adapter.entities.BalpResponse;
import hr.etna.api.adapter.util.BalpAdapterException;
import hr.etna.api.adapter.util.BalpHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class AbstractBalpService implements BalpService{
	
	
	private static final Properties prop = new Properties();
	protected ExecutorService executor;
	private Map<String, BalpMessage> failedMessagesMap = new HashMap<String, BalpMessage>();  
	
	private static String REST_URL; 
	private static String API_SECRET; 
	private static String APP_NAME; 
	private static String APP_TOKEN; 
	private static boolean LOG_FAILS = true;
	protected static boolean RESEND_AFTER_NEXT_SUCCESSFULL_CALL = false;
	private static int NUMBER_OF_THREDS = 1;
	
	
	private final static String URL_PROPERTY = "balp.adapter.rest.url";
	private final static String API_SECRET_PROPERTY = "balp.adapter.api.secret";
	private final static String APP_NAME_PROPERTY = "balp.adapter.application.name";
	private final static String APP_TOKEN_PROPERTY = "balp.adapter.application.token";
	private final static String LOG_FAILED_CALLS_PROPERTY = "balp.adapter.log.log_failed_balp_calls";
	private final static String NUMBER_OF_LOG_THREDS_PROPERTY = "balp.adapter.log.number_of_threds";
	private final static String RESEND_AFTER_NEXT_SUCCESSFULL_CALL_PROPERTY = "balp.adapter.resend_after_next_successfull_call";

    /**
     * Constructor
     *
     * @throws BalpAdapterException
     */
    public AbstractBalpService() throws BalpAdapterException {
        InputStream input = null;
        try {
            input = Balp.class.getClassLoader().getResourceAsStream("balp-adapter.properties");
			prop.load(input);
			
			String restUrl = prop.getProperty(URL_PROPERTY);
			String apiSecret = prop.getProperty(API_SECRET_PROPERTY);
			String appName = prop.getProperty(APP_NAME_PROPERTY);
			String appToken = prop.getProperty(APP_TOKEN_PROPERTY);
			
			// others
			String p_logFails = prop.getProperty(LOG_FAILED_CALLS_PROPERTY);
			String p_numberOfThreds = prop.getProperty(NUMBER_OF_LOG_THREDS_PROPERTY);
			String p_retryAfterNextSuccessfullCall = prop.getProperty(RESEND_AFTER_NEXT_SUCCESSFULL_CALL_PROPERTY);
			
			
			boolean logFails = false;
			if(p_logFails!=null && p_logFails.equalsIgnoreCase("true"))
				logFails = Boolean.TRUE;
			
			boolean retryAfterNextSuccessfullCall = RESEND_AFTER_NEXT_SUCCESSFULL_CALL;
			if(p_retryAfterNextSuccessfullCall!=null && p_retryAfterNextSuccessfullCall.equalsIgnoreCase("true"))
				retryAfterNextSuccessfullCall = true;
				
			int numberOfThreds = NUMBER_OF_THREDS;
			if(p_numberOfThreds != null && p_numberOfThreds.length()>0) {
				try {  
				    numberOfThreds = Integer.parseInt(p_numberOfThreds);  
				} catch(NumberFormatException nfe) {/*ignore*/}  
			} 

			if(restUrl == null || restUrl.length()==0)
				throw new BalpAdapterException("BALP ADAPTER: [ERROR] Missing mandatory property: " + URL_PROPERTY);
			
			if(apiSecret == null || apiSecret.length()==0)
				throw new BalpAdapterException("BALP ADAPTER: [ERROR] Missing mandatory property: " + API_SECRET_PROPERTY);
			
			if(appName == null || appName.length()==0)
				throw new BalpAdapterException("BALP ADAPTER: [ERROR] Missing mandatory property: " + APP_NAME_PROPERTY);
			
			if(appToken == null || appToken.length()==0)
				throw new BalpAdapterException("BALP ADAPTER: [ERROR] Missing mandatory property: " + APP_TOKEN_PROPERTY);
			
			
			//set properties
			REST_URL = restUrl;
			API_SECRET = apiSecret;
			APP_NAME = appName;
			APP_TOKEN = appToken;
			LOG_FAILS = logFails;
			NUMBER_OF_THREDS = numberOfThreds;
			RESEND_AFTER_NEXT_SUCCESSFULL_CALL = retryAfterNextSuccessfullCall;

			//init pooler for concurrent calls
			executor = Executors.newFixedThreadPool(NUMBER_OF_THREDS);
			
		} catch (IOException ex) {
			String msg = "BALP ADAPTER: [WARNING] Properties file 'balp-adapter.properties' missing! \n **hr.etna.api.adapter.Balp adapter cannot be initialized.";
			System.err.println(msg);
			throw new BalpAdapterException(msg);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}


    public Object fireRestCall(BalpMessage msg) {
        HttpURLConnection httpConnection = null;
        try {
            BalpResponse reto;

            //set apptoken and name
            if (msg.getApplicationName() == null)
                msg.setApplicationName(APP_NAME);

            if(msg.getApplicationToken()==null)
				msg.setApplicationToken(APP_TOKEN);

			httpConnection = (HttpURLConnection) new URL(REST_URL).openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Content-Type", "application/json");
			httpConnection.setRequestProperty("X-secret", API_SECRET);

			OutputStream os = httpConnection.getOutputStream();
			
			os.write(msg.toJson().getBytes());
			os.flush();

			if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.err.println("--------------------------- balp http nok --------------------------------");
                System.err.println("[BALP] FIRE FAILED:\n" + msg.toJson());
                System.err.println("--------------------------- end  http nok --------------------------------");
                msg.increaseRetryCounter();
                reto = BalpResponse.NOK(new Exception(httpConnection.getResponseCode() + ": " + BalpHelper.streamToString(httpConnection.getInputStream())), msg);
                if (LOG_FAILS)
                    handleBalpResponse(reto);
			}else {
                System.err.println("--------------------------- balp http ok--------------------------------");
                System.err.println("[BALP] FIRE SUCCESS:\n" + msg.toJson());
                System.err.println("--------------------------- end http ok  --------------------------------");
                reto = BalpResponse.OK(msg.getCorrelationId(), msg);
            }
            handleBalpResponse(reto);
            return null;

		} catch (Exception e) {
			//e.printStackTrace();
			msg.increaseRetryCounter();
			BalpResponse balpResponse = BalpResponse.NOK(e, msg);
			if (LOG_FAILS)
				handleBalpResponse(balpResponse);
			return null;
		} finally {
			if (httpConnection != null) {
				httpConnection.disconnect();
			}
		}
	}
	
	@Override
	public Map<String, BalpMessage> getFailedUnsentBalpMessages(){
        return this.failedMessagesMap;
    }

    @Override
    public void resetFailedUnsentBalpMessages() {
        this.failedMessagesMap = new HashMap<String, BalpMessage>();
    }

}
