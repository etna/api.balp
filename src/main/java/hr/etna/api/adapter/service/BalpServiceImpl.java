package hr.etna.api.adapter.service;

import hr.etna.api.adapter.entities.BalpMessage;
import hr.etna.api.adapter.entities.BalpResponse;
import hr.etna.api.adapter.util.BalpAdapterException;

import java.util.concurrent.Callable;

public class BalpServiceImpl extends AbstractBalpService {


    public BalpServiceImpl() throws BalpAdapterException {
        super();
    }

    @Override
    public void fire(final BalpMessage msg) {
        System.out.println("msg: " + msg);

        Callable<Object> call = new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return fireRestCall(msg);
            }
        };
        executor.submit(call);
    }

    @Override
    public void handleBalpResponse(BalpResponse balpResponse) {
        BalpMessage msg = balpResponse.getBalpMessage();
        if (balpResponse.isSuccessful()) {
            // ako je poruka uspješna probaj poslati i iduću iz mape, provjeri da nije u mapi, ako je, makni je
            this.getFailedUnsentBalpMessages().remove(balpResponse.getMessageId());
            if (RESEND_AFTER_NEXT_SUCCESSFULL_CALL) {
                if (this.getFailedUnsentBalpMessages().size() > 0) {
                    BalpMessage nextMsg = this.getFailedUnsentBalpMessages().values().iterator().next();
                    System.out.println("[BALP] [INFO] Resend failed balp message [id: " + nextMsg.getCorrelationId() + "] - Number of retries: " + nextMsg.getRetryCounter() + " ");
                    fire(nextMsg);
                }
            }
        } else {
            //ako je poruka pukla, spremi je
            //msg.increaseRetryCounter();
            this.getFailedUnsentBalpMessages().put(balpResponse.getMessageId(), msg);
        }
    }

}
