package hr.etna.api.adapter.service;

import hr.etna.api.adapter.entities.BalpMessage;
import hr.etna.api.adapter.entities.BalpResponse;

import java.util.Map;

public interface BalpService {

    public void fire(BalpMessage msg);

    public Object fireRestCall(BalpMessage msg);

    public void handleBalpResponse(BalpResponse balpResponse);

    public Map<String, BalpMessage> getFailedUnsentBalpMessages();

    public void resetFailedUnsentBalpMessages();


}
