package hr.etna.api.adapter;

import hr.etna.api.adapter.entities.ActivityPayload;
import hr.etna.api.adapter.entities.BalpMessage;
import hr.etna.api.adapter.entities.BalpUser;
import hr.etna.api.adapter.util.BalpAdapterException;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by mvidovic on 18.03.14..
 */
public class Main {

    /**
     * Main call
     *
     * @param args blah
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        ActivityPayload ap = new ActivityPayload("PRVKA", 23424l);
        ActivityPayload ap3 = new ActivityPayload("TROJKA", 23424l);
        ap3.setOptions(new HashMap<String, Object>());
        ap3.getOptions().put("imenko", "Jimenez");
        ap3.getOptions().put("prezimenka", "Jimenezova");

        BalpUser u1 = new BalpUser("1", "mvidovic");
        BalpUser u3 = new BalpUser("2", "pperic");
        BalpUser u2 = new BalpUser("3", "ccrcic");

        try {

            // msg
            BalpUser user1 = new BalpUser("1", "Slavko Mirić");
            BalpUser user2 = new BalpUser("2", "Mirko Perić");
            BalpUser user3 = new BalpUser("3", "Tvrtko Matić");

            BalpMessage msg1 = new BalpMessage(1l, user1, BalpMessage.EventType.USER_ACTIVITY, "UA1");
            BalpMessage msg2 = new BalpMessage(2l, null, BalpMessage.EventType.ACTIVITY, ap);
            BalpMessage msg3 = new BalpMessage(3l, user3, BalpMessage.EventType.GENERIC, ap3);
            BalpMessage msg4 = new BalpMessage(4l, user1, BalpMessage.EventType.USER_ACTIVITY, "UA2");

            Map<String, Object> options = new HashMap<String, Object>();
            options.put("ime", "mario");
            options.put("godine", 33);
            options.put("datum_promjene", new Date());
            options.put("tagovi", new Object[]{"crvena", "bijela", "plava", null, "crna"});

            msg4.getActivityPayload().setOptions(options);

            Balp.fire(msg1);
            System.out.println("Okinut fire msg: 1");
            Balp.fire(msg2);
            System.out.println("Okinut fire msg: 2");
            Balp.fire(msg3);
            System.out.println("Okinut fire msg: 3");
            Balp.fire(msg4);
            System.out.println("Okinut fire msg: 4");

            Thread.currentThread().sleep(4000);
            System.out.println("------- end -----");
            System.out.println("Failed balp messages size: " + Balp.service().getFailedUnsentBalpMessages().size());
            Iterator<BalpMessage> failed = Balp.getFailedUnsentBalpMessages().values().iterator();
            while (failed.hasNext()) {
                BalpMessage fm = failed.next();
                System.out.println("FAILED MSG : " + fm.getCorrelationId() + " retries: " + fm.getRetryCounter());
            }


        } catch (BalpAdapterException e) {
            e.printStackTrace();
        }
        System.out.println("---> 1");
    }
}
