package hr.etna.api.adapter.util;

public class BalpAdapterException extends Exception {

	private static final long serialVersionUID = -2948518173949879369L;

	public BalpAdapterException() {
	}

	public BalpAdapterException(String message) {
		super(message);
	}

	public BalpAdapterException(String message, Throwable cause) {
		super(message, cause);
	}
}
