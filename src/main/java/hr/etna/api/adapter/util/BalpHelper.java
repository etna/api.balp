package hr.etna.api.adapter.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

public class BalpHelper {

	public static String createMessageId() {
		String p1, p2, p3;
		try {
			p1 = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			p1 = "n/a";
		}

		p2 = ((Long) Thread.currentThread().getId()).toString();
		p3 = UUID.randomUUID().toString();

		return p1 + "#" + p2 + "#" + p3;
	}

	public static String streamToString(InputStream is) throws Exception {
        if (is != null) {
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }
	
	
	
}
