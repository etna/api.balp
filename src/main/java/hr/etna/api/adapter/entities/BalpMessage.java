package hr.etna.api.adapter.entities;

import java.io.Serializable;


/**
 * 
 * BalpMessage
 * 
 * @author mvidovic
 *
 */
public class BalpMessage implements Serializable {
	
	private static final long serialVersionUID = -7052799595173463240L;

	public enum EventType {ACTIVITY, GENERIC, USER_ACTIVITY}

	private String correlationId;
	private long time;
	private BalpUser user;
	private String applicationName;
	private String applicationToken;
	private EventType eventType = EventType.GENERIC;
	private ActivityPayload activityPayload;
	
	private int retryCounter = 0;

    /**
     * Default constructor
     */
    public BalpMessage() {
    }

    /**
     * Creates BalpMessage with activityCode and without CorrelationId
     *
     * @param time         head.time
     * @param user         head.user
     * @param eventType    head.eventType
     * @param activityCode payload.activityCode
     */
    public BalpMessage(long time, BalpUser user, EventType eventType, String activityCode) {
        ActivityPayload activityPayload = new ActivityPayload(activityCode);
        init("", time, user, eventType, activityPayload);
    }

    /**
     * Creates BalpMessage with activityCode
     *
     * @param correlationId correlationId or messageId
     * @param time          head.time
     * @param user          head.user
     * @param eventType     head.eventType
     * @param activityCode  payload.activityCode
     */
    public BalpMessage(String correlationId, long time, BalpUser user, EventType eventType, String activityCode) {
        ActivityPayload activityPayload = new ActivityPayload(activityCode);
        init(correlationId, time, user, eventType, activityPayload);
    }

    /**
     * Creates BalpMessage without correlationId
     *
     * @param time            head.time
     * @param user            head.user
     * @param eventType       head.eventType
     * @param activityPayload payload
     */
    public BalpMessage(long time, BalpUser user, EventType eventType, ActivityPayload activityPayload) {
        init("", time, user, eventType, activityPayload);
    }

    /**
     * Default constructor: Creates BalpMessage
     *
     * @param correlationId   correlationId or messageId
     * @param time            head.time
     * @param user            head.user
     * @param eventType       head.eventType
     * @param activityPayload payload
     */
    public BalpMessage(String correlationId, long time, BalpUser user, EventType eventType, ActivityPayload activityPayload) {
        this.init(correlationId, time, user, eventType, activityPayload);
    }

    private void init(String correlationId, long time, BalpUser user, EventType eventType, ActivityPayload activityPayload) {
		this.correlationId = correlationId;
		this.time = time;
		this.user = user;
		this.eventType = eventType;
		this.activityPayload = activityPayload;		
	}

	
	// setters and getters
	
	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public BalpUser getUser() {
		return user;
	}

	public void setUser(BalpUser user) {
		this.user = user;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getApplicationToken() {
		return applicationToken;
	}

	public void setApplicationToken(String applicationToken) {
		this.applicationToken = applicationToken;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public ActivityPayload getActivityPayload() {
		return activityPayload;
	}

	public void setActivityPayload(ActivityPayload activityPayload) {
        this.activityPayload = activityPayload;
    }

    public int getRetryCounter() {
        return retryCounter;
    }

    public void setRetryCounter(int retryCounter) {
        this.retryCounter = retryCounter;
    }

    public void increaseRetryCounter() {
        this.retryCounter++;
    }

    /**
     * Returns BalpMessage in JSON
     *
     * @return Json String
     */
    public String toJson() {
		StringBuilder json = new StringBuilder();

		//START JSON
		json.append("{");
		
		// MESSAGE HEADER
		json.append("\"head\": {");
		json.append(	"\"correlationId\": \"" + ((this.correlationId!=null)?this.correlationId:"") + "\",");
		json.append(	"\"time\": " + this.time + ",");
		
		// USER
		json.append((this.getUser()!=null)?this.getUser().toJson():"");

		//separator
		json.append(",");
		
		json.append(	"\"applicationName\": \""+ this.applicationName +"\",");
		json.append(	"\"applicationToken\": \"" + this.applicationToken + "\",");
		
		
		json.append(	"\"eventType\": \"" + ((this.eventType==EventType.USER_ACTIVITY)?"user-activity":this.eventType.toString().toLowerCase()) + "\"");
		json.append("}");
		
		//separator
		json.append(",");
		
		// MESSAGE BODY
		json.append(this.activityPayload.toJson());

		//END JSON
		json.append("}");
		
		return json.toString();
	}

}
