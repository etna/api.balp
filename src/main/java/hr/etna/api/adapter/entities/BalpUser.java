package hr.etna.api.adapter.entities;

import java.io.Serializable;

public class BalpUser implements Serializable {
	
	private static final long serialVersionUID = 2134004676071161907L;

	private String id;
	private String name;

	public BalpUser() {
		// default
	}

	public static String createUserId(BalpUser user) {
		return user.getId().trim() + "-" + user.getName().trim();
	}

	public BalpUser(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toJson() {
		StringBuilder json = new StringBuilder();
		json.append("\"user\": {");
		
		if(this.getId()!=null)
			json.append("\"id\": \"").append(this.id).append("\",");
		
		if(this.name!=null && this.name.length()>0)
			json.append("\"name\": \"").append(this.name).append("\"");
		
		json.append("}");
		return json.toString();
	}
}
