package hr.etna.api.adapter.entities;

import java.io.Serializable;

public class BalpResponse implements Serializable {
	
	private static final long serialVersionUID = 9197163615137560916L;
	
	private Boolean success = false;
	private Throwable t;
    private String messageId;
    private BalpMessage balpMessage;

    private BalpResponse(Boolean success, String messageId, Throwable t){
        this.success = success;
        this.messageId = messageId;
        this.t = t;
    }
    
    private BalpResponse(Boolean success, String messageId, Throwable t, BalpMessage balpMessage){
        this.success = success;
        this.messageId = messageId;
        this.t = t;
        this.balpMessage = balpMessage;
    }

    public static BalpResponse OK(String messageId){
        return new BalpResponse(true, messageId, null);
    }
    
    public static BalpResponse OK(String messageId, BalpMessage balpMessage){
        return new BalpResponse(true, messageId, null, balpMessage);
    }

    public static BalpResponse NOK(Throwable t){
        return new BalpResponse(false, "n/a", t);
    }
    
    public static BalpResponse NOK(Throwable t, BalpMessage balpMessage){
        return new BalpResponse(false, "n/a", t, balpMessage);
    }

	public Boolean isSuccessful() {
		return success;
	}

	public Throwable getException() {
		return t;
	}

    public String getMessageId(){
        return messageId;
    }

	public BalpMessage getBalpMessage() {
		return balpMessage;
	}

	public void setBalpMessage(BalpMessage balpMessage) {
		this.balpMessage = balpMessage;
	}

}
