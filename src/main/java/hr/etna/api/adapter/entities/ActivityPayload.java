package hr.etna.api.adapter.entities;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ActivityPayload {

	private String activityCode = null;
	private Long duration = null;
    private Map<String, Object> options = new HashMap<String, Object>();

    public ActivityPayload(String activityCode) {
        this.activityCode = activityCode;
    }

    public ActivityPayload(String activityCode, Long duration) {
		this.activityCode = activityCode;
		this.duration = duration;
	}
	
	public String getActivityCode() {
		return activityCode;
	}
	public void setActivityCode(String activityCode) {
		this.activityCode = activityCode;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}

    public Map<String, Object> getOptions() {
        return options;
    }

    public void setOptions(Map<String, Object> options) {
        this.options = options;
    }

    /**
     * Converts object to json
     *
     * @return json String
     */
    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("\"payload\":{");
        boolean hasBefore = false;
        
        if(this.activityCode!=null && this.activityCode.trim().length()!=0) {
        	if(hasBefore)
        		json.append(",");
        	json.append("\"activityCode\": \"").append(this.activityCode).append("\"");
        	hasBefore = true;
        }

        if(this.duration!=null) {
        	if(hasBefore)
        		json.append(",");
        	json.append("\"duration\": ").append(this.duration.longValue());
        	hasBefore = true;
        }
        
        if(this.options!=null && this.options.keySet().size()>0){
        	if(hasBefore)
        		json.append(",");
        	
            json.append("\"options\": {");
            int counter = 0;
            for(String key: this.options.keySet()){
                if(counter > 0){
                    json.append(",");
                }
                json.append("\"" + key + "\": ");

                Object val = this.options.get(key);

                if (val instanceof Integer) {
                    json.append(((Integer) val).intValue());
                } else if (val instanceof Double) {
                    json.append(((Double) val).doubleValue());
                } else if (val instanceof Date) {
                    json.append(((Date) val).getTime());
                } else if (val instanceof Long) {
                    json.append(((Long) val).longValue());
                } else if (val instanceof Boolean) {
                    json.append(((Boolean) val).booleanValue());
                } else if (val instanceof Object[]) {
                    Object[] aVal = (Object[]) val;
                    String aValStr = "";
                    boolean isFirst = true;
                    for (Object o : aVal) {
                        if (isFirst) {
                            aValStr += (o != null) ? "\"" + o + "\"" : "null";
                            isFirst = false;
                        } else {
                            aValStr += (o != null) ? ",\"" + o + "\"" : ", null";
                        }
                    }
                    json.append("[" + aValStr + "]");
                } else {
                    json.append("\"" + val + "\"");
                }
                counter++;
            }
            json.append("}");
            hasBefore = true;
        }
        json.append("}");
        return json.toString();
	}

}
